package co.micol.hello;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
//1) web.xml에 직접 등록
//2) @WebServlet 사용 (annotaion)

//@WebServlet("/FirstServlet")   //=> 나는 Servlet에 올라가야하는 거에요 알려줌(미리 서버(컨테이너)에 올라가서 최초로 호출하면 컴파일해서 실행)
                                 // 이걸 막으면 404오류(호출경로오류 => 호출이름오타/경로맞는지 확인)
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    //doget =>  데이터가 get 방식으로 호출 (url뒤에 변수로 실려 가는것(?id(변수) = 1(값))) => 사용자제(다 보여서 안전하지 않음)
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8"); 
		
		response.getWriter().append("서블릿이 동작된다.<br>").append(request.getContextPath());   //getContextPath() =>/Hello
		response.getWriter().append("<br>행복한 JSP 프로그래머");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//getpost => 데티어가 post방식으로 호출 (html의 form에 담겨서 가는것)
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
