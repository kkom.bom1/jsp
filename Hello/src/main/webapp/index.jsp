<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1> wellCome to JSP </h1>
	<%!
		String message = "자바코드로 적성한것";
	%>
	<% 
	   for(int i=0; i<10; i++){ %>
		   ${1+1} <br>
	<%  }  %>
	<%= message %>
	
	<br>
	<a href="FirstServlet">서블릿호출</a>
	<br><!-- get방식 -->
	<a href="login.jsp?name=홍길동&password=1234">로그인</a><br>
	<a href="Login?name=홍길동&password=1234">로그인서블릿</a><br>
	<a href="loginform.jsp">로그인폼호출하기</a>
	
</body>
</html>

<%--jsp태그
<%@    %> => 지시자(컨테이너(서버)에게 알려주는것
<% 자바코드 %>
<%! 자바선언문(함수정의/ 클래스 정의/ 변수정의...) %>
<%=   %> => 화면(브라우저) 출력  --%>

