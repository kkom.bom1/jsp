package co.micol.prj.web;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.MainCommand;
import co.micol.prj.common.Command;
import co.micol.prj.member.command.AjaxMemberIdCheck;
import co.micol.prj.member.command.MemberDelete;
import co.micol.prj.member.command.MemberEdit;
import co.micol.prj.member.command.MemberJoin;
import co.micol.prj.member.command.MemberJoinForm;
import co.micol.prj.member.command.MemberList;
import co.micol.prj.member.command.MemberLogout;
import co.micol.prj.member.command.MemberSelect;
import co.micol.prj.member.command.MemberUpdate;
import co.micol.prj.notice.command.NoticeInsert;
import co.micol.prj.notice.command.NoticeInsertForm;
import co.micol.prj.notice.command.NoticeList;
import co.micol.prj.notice.command.NoticeSelect;
import co.micol.prj.member.command.MemberLogin;
import co.micol.prj.member.command.MemberLoginForm;

/**
 * Servlet implementation class FrontController
 */
//@WebServlet("*.do")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private HashMap<String, Command> map = new HashMap<String, Command>();

	public FrontController() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		// 명령집단 map.put(k,v) 로 보관
		map.put("/main.do", new MainCommand()); //첨실행 페이지
		map.put("/memberList.do", new MemberList()); //멤버목록보기
		map.put("/memberJoinForm.do", new MemberJoinForm());//회원가입 폼
		map.put("/AjaxMemberIdCheck.do", new AjaxMemberIdCheck()); //회원아이디 중복
		map.put("/memberJoin.do", new MemberJoin()); //회원가입 처리
		map.put("/memberLoginForm.do", new MemberLoginForm()); //로그인호출 
		map.put("/memberLogin.do", new MemberLogin()); //로그인 ㅊ처리
		map.put("/memberLogout.do", new MemberLogout()); //로그인 처리
		map.put("/memberSelect.do", new MemberSelect()); //멤버 한명조회
		map.put("/memberEdit.do", new MemberEdit()); //멤버수정폼 호출
		map.put("/memberDelete.do", new MemberDelete()); //멤버삭제
		map.put("/memberUpdate.do", new MemberUpdate()); //멤버 수정
		map.put("/noticeInsertForm.do", new NoticeInsertForm()); //공지사항등록폼
		map.put("/noticeList.do", new NoticeList()); //공지사항목록
		map.put("/noticeSelect.do", new NoticeSelect()); //공지사항상세보기
		map.put("/noticeInsert.do", new NoticeInsert()); //공지사항 등록
		
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 요청을 분석, 실행, 결과를 돌려주는 곳
		request.setCharacterEncoding("utf-8");
		
		String uri = request.getRequestURI();
		String contextPath = request.getContextPath();
		String page = uri.substring(contextPath.length());
		
		Command command = map.get(page); //value값을 돌려줌
		String viewPage = command.exec(request, response);
		
		//view resolve - .do / ajax / 기본으로 돌려받을때
		//결과를 어떤페이지에 뿌려줄지 
		if(!viewPage.endsWith(".do")) {
			if(viewPage.startsWith("Ajax:")) {
				//ajax - index의 컨텐츠 타입
				response.setContentType("text/html; charset=UTF-8");
				response.getWriter().print(viewPage.substring(5)); //'ajax:' 뒤부터 데이터임
				return;
			}else if(!viewPage.endsWith(".tiles")){
				//기본으로 돌려받을때
				viewPage = "WEB-INF/views/"+ viewPage + ".jsp"; //타일즈 적용안하는 것
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher(viewPage);
			dispatcher.forward(request, response);
		}else {
			//.do로 받을때
			response.sendRedirect(viewPage);
		}
	}

}
