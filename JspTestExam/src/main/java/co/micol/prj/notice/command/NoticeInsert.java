package co.micol.prj.notice.command;

import java.io.File;
import java.io.IOException;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class NoticeInsert implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 공지사항 글등록하기 MutipartRequest를 처리해야한다.(cos라이브러리를 이용)
		NoticeService dao = new NoticeServiceImpl();
		NoticeVO vo = new NoticeVO();
		
		//String saveDir = request.getServletPath()  ???????
		//String saveDir = "C:\\Users\\admin\\git\\jsp\\20221207\\src\\main\\webapp\\attech\\";
		int maxSize = 1024 * 1024 * 1024; // 최대 10m까지 업로드

		try {
			// 파일 업로드시 request객체를 대체한다. 
			// 멀티팔트리퀘스트객체 초기화 할때 DefaultFileRenamePolicy()=> 동일한 파일명 존재하면 뒤에 숫자 붙여줌
			//이미 이 객채 생성 순간 파일 메모리에 올라옴
			MultipartRequest multi = new MultipartRequest(request, saveDir, maxSize, "utf-8", new DefaultFileRenamePolicy());
			
			vo.setNoticeWriter(multi.getParameter("noticeWriter"));
			vo.setNoticeDate(Date.valueOf(multi.getParameter("noticeDate")));
			vo.setNoticeTitle(multi.getParameter("noticeTitle"));
		    vo.setNoticeSubject(multi.getParameter("noticeSubject"));
		    
		    
			//이순간에 이미 저장
			String ofileName = multi.getOriginalFileName("nfile"); //원본파일명
			String pfileName = multi.getFilesystemName("nfile"); //실제저장되는 파일 이름
			
			if(ofileName != null) {
				vo.setNoticeFile(ofileName);
				pfileName = saveDir + pfileName; //저장directory와 저장명
				vo.setNoticeFileDir(pfileName);
				
			}
			
			int n = dao.noticeInsert(vo);
			if(n != 0) {
				request.setAttribute("message", "공지사항이 등록되었습니다.");
			}else {
				request.setAttribute("message", "공지사항 등록실패했습니다.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "notice/noticeMessage.tiles";
	}

}
