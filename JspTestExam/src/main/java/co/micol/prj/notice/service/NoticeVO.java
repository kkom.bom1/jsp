package co.micol.prj.notice.service;

import java.sql.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeVO {
	private int noticeId;
	private String noticeWriter;
	private Date noticeDate;
	private String noticeTitle;
	private String noticeSubject;
	private int noticeHit;            //저장할때 여기까지만
	
	private int attechId;  //join을 위한 확장
	private String noticeFile;
	private String noticeFileDir;     //불러올땐 전부 다쓴다.
	
}
