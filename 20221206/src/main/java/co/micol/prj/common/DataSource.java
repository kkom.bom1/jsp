package co.micol.prj.common;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
//datesource => 인터페이스 //관례적으로 DAO를 데이터소스라 쓴다
public class DataSource { //클래스 DAO
	//싱글톤클래스 => 전체에서 하나만쓰는건 싱글톤 해줌
	private static SqlSessionFactory sqlSessionFactory;  //돌려줄 리턴값
	private DataSource() {};  //바깥에서 생성자 ㄴㄴ => 초기화 하지 못하게
	
	public static SqlSessionFactory getInstance() { //인스턴스 이용해서 가져갈 수 있도록
		String resource = "config/mybatis-config.xml";

		try {
			InputStream inputStream = Resources.getResourceAsStream(resource);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		return sqlSessionFactory;
	}
}
