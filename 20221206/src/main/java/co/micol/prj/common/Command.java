package co.micol.prj.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command { //공통으로 사용
	//리퀘스트와 리스폰스 값을 받아서 스트링으로 돌려주는 메소드
	String exec(HttpServletRequest request, HttpServletResponse response);
}
