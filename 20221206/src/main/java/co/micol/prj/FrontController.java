package co.micol.prj;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.micol.prj.common.Command;
import co.micol.prj.member.command.MemberList;

@WebServlet("*.do")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// 명령저장할수 있도록 해쉬맵
	private HashMap<String, Command> map = new HashMap<String, Command>();

	// 요청분석하고 분석된 요청에 따라 실행된 커맨드 수행, 돌려줄jsp찾아서 결과 리턴
	public FrontController() {
		super();
	}

	public void init(ServletConfig config) throws ServletException {
		// 명령(Command)를 저장하는 영역
		map.put("/main.do", new MainCommand()); //첨페이지 명령
		map.put("/memberList.do", new MemberList()); //멤버목록보기
		//인터페이스는 자기 자신을 초기화 못함. 그러니 구현체로부터 초기화 (new MainCommand())
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Controller 본체
		request.setCharacterEncoding("utf-8");// 한글깨짐 방지
		
		//브라우저를 통해 요청 분석 -uri와 contexpath알아야 실제요청페이지 앎
		String uri = request.getRequestURI(); // uri값을 읽어냄
		String contextPath = request.getContextPath(); // contextpath를 읽어온다.
		String page = uri.substring(contextPath.length()); // 실제요청명을 구한다.
		//ex) localhost(/20221206/index.jsp) -> uri
		//ex) localhost(/20221206)/index.jsp -> contextpath
		//ex) localhost/20221206/(index.jsp) -> 실제요청명
		// page => /index.jsp
		
		//적절한 커맨드 찾아서 커맨드 실행
		Command command = map.get(page); //수행할 command찾고
		String viewPage = command.exec(request, response); //찾은 command 실행
		
		//커맨드 동작이 되면 나에게 돌려줄 페이지를 찾는것.(view resolve)
		if(!viewPage.endsWith(".do")) { //스트링으로 돌아오는 값이 .do가 아니면
			//Ajax처리하는 루틴
			//views밑 .jsp를 찾는다.
			viewPage = "WEB-INF/views/" + viewPage+".jsp";
			
			//던진 리퀘스트를 최종페이지 까지 전달 
			RequestDispatcher dispatcher = request.getRequestDispatcher(viewPage);
			dispatcher.forward(request, response);
		}else {
			//새로운 페이지로 가는것
			response.sendRedirect(viewPage);
		}
		
	}

}
